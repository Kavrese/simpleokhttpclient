package com.wsr.okhttpapp

import com.google.gson.Gson
import okhttp3.*
import java.io.IOException


object Connection{
    val okHttpClient = OkHttpClient()
    val requests = Requests()
}

class Requests{
    private val baseUrl = "https://api.chucknorris.io/jokes/"

    fun random(): Request = Request.Builder().url("$baseUrl/random").build()
    fun categories(): Request = Request.Builder().url("$baseUrl/categories").build()
}

class ConnectionController {
    companion object {
        inline fun <reified T> Request.push(onGetData: OnGetData<T>){
            Connection.okHttpClient.newCall(this).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    onGetData.onFail(e.localizedMessage ?: e.message ?: "Unknown error")
                }

                override fun onResponse(call: Call, response: Response) {
                    if (response.body() != null){
                        onGetData.getData(Gson().fromJson<T>(response.body()!!.string(), T::class.java))
                    }else{
                        onGetData.onFail("${response.code()} - ${response.message()}, Empty body")
                    }
                }
            })
        }
    }
}

interface OnGetData <T>{
    fun getData(body: T)
    fun onFail(message: String)
}