package com.wsr.okhttpapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.wsr.okhttpapp.ConnectionController.Companion.push
import com.wsr.okhttpapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var adapterCategories: AdapterCategories

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.refresh.setOnClickListener {
            pushRequests()
        }

        adapterCategories = AdapterCategories(mutableListOf())
        binding.recCategories.apply {
            adapter = adapterCategories
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        pushRequests()
    }

    private fun pushRequests(){
        pushJoke()
        pushCategories()
    }

    private fun pushJoke(){
        Connection.requests.random().push(object: OnGetData<ModelJoke>{
            override fun getData(body: ModelJoke) {
                binding.joke.text = body.value
            }

            override fun onFail(message: String) {
                Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun pushCategories(){
        Connection.requests.categories().push(object: OnGetData<List<String>>{
            override fun getData(body: List<String>) {
                runOnUiThread {
                    adapterCategories.replaceData(body)
                }
            }

            override fun onFail(message: String) {
                Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
            }
        })
    }
}