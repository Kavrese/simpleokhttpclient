package com.wsr.okhttpapp

data class ModelJoke(
    val id: String,
    val value: String
)