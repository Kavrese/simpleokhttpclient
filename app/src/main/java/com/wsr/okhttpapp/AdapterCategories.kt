package com.wsr.okhttpapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wsr.okhttpapp.databinding.ItemCategoryBinding

class AdapterCategories(private val categories: MutableList<String>):
    RecyclerView.Adapter<AdapterCategories.CategoryViewHolder>() {

    class CategoryViewHolder(private val itemBinding: ItemCategoryBinding): RecyclerView.ViewHolder(itemBinding.root){
        fun bind(category: String){
            itemBinding.category.text = category
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(categories[position])
    }

    override fun getItemCount(): Int = categories.size

    fun replaceData(newCategories: List<String>){
        categories.clear()
        categories.addAll(newCategories)
        notifyDataSetChanged()
    }
}